package pojo;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 模板
 *
 * @author admin
 */
public class Prient implements Printable {
    // 菜品集合
    public static List<Test> testList = new ArrayList<Test>();

    // 设置小票打印
    @Override
    public int print(Graphics g, PageFormat pf, int page)
            throws PrinterException {
        if (page > 0) {
            return NO_SUCH_PAGE;
        }
        Graphics2D g2d = (Graphics2D) g;
        // 设置颜色
        g2d.setColor(Color.BLACK);
        //模式  字体   字体大小
        g2d.setFont(new Font("Default", Font.PLAIN, 16));
        // 参数1：显示内容 参数2：横向偏移 参数3：纵向偏移
        g2d.drawString("打印清单", 100, 50);
        g2d.drawString("------------------------------------------------", 40, 70);
        g2d.setFont(new Font("Default", Font.PLAIN, 12));
        g2d.drawString("测试员：自定义", 40, 90);
        g2d.drawString("员工编号：自定义", 40, 110);
        g2d.drawString("订单编号：自定义", 40, 130);
        g2d.drawString("测试时间：" + new Date(), 40, 150);
        g2d.setFont(new Font("Default", Font.PLAIN, 16));
        g2d.drawString("------------------------------------------------", 40, 170);
        g2d.drawString("商品             单价             小计", 40, 190);
        g2d.setFont(new Font("Default", Font.PLAIN, 12));
        int H1 = 190;
        double zmoney = 0;
        int count = 0;
        for (Test test : testList) {
            count = count + 1;
            H1 = H1 + 20;
            zmoney = test.getMoney() * test.getNum() + zmoney;
            g2d.drawString(count + "." + test.getName() + "(" + test.getNum()
                    + "件)     ￥" + test.getMoney() + "     ￥" + test.getMoney() * test.getNum(), 40, H1);
        }
        g2d.setFont(new Font("Default", Font.PLAIN, 16));
        g2d.drawString("------------------------------------------------", 40, H1 + 20);
        g2d.setFont(new Font("Default", Font.PLAIN, 12));
        g2d.drawString("合计：￥" + zmoney, 40, H1 + 40);
        g2d.drawString("优惠金额：￥" + 20, 40, H1 + 60);
        g2d.drawString("应收：￥" + (zmoney - 20), 40, H1 + 80);
        g2d.drawString("实收：￥" + zmoney, 40, H1 + 100);
        g2d.drawString("找零：￥" + 20, 40, H1 + 120);
        g2d.drawString("员工：" + "打印测试", 40, H1 + 140);
        g2d.drawString("1谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 160);
        g2d.drawString("2谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 180);
        g2d.drawString("3谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 200);
        g2d.drawString("4谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 360);
        g2d.drawString("5谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 460);
        g2d.drawString("6谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 560);
        g2d.drawString("7谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, 0);
        g2d.drawString("8谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, 760);
        g2d.drawString("9谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, 850);
        g2d.drawString("0谢谢惠顾，欢迎下次光临！谢谢惠顾，欢迎下次光临！", 80, H1 + 160);
        return PAGE_EXISTS;
    }

    public static List<Test> getTestList() {
        return testList;
    }

    public static void setTestList(List<Test> testList) {
        Prient.testList = testList;
    }
}
