import pojo.Prient;
import pojo.Test;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.MediaName;
import javax.print.attribute.standard.PrinterName;
import java.awt.print.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 打印测试类
 *
 * @author admin
 */
public class MainTest {

    public static void main(String[] args) {
        //这里是测试用的数据，自己定义就好了
        List<Test> testList = new ArrayList<Test>();
        Test t1 = new Test("小米", 23.00, 2);
        Test t2 = new Test("三星", 23.00, 4);
        Test t3 = new Test("华为", 24.00, 3);
        Test t4 = new Test("OPPO", 23.00, 2);
        Test t5 = new Test("VIVO", 26.00, 1);
        Test t6 = new Test("魅族", 26.00, 1);
        Test t7 = new Test("荣耀", 26.00, 1);
        Test t8 = new Test("苹果", 26.00, 1);
        Test t9 = new Test("中兴", 26.00, 1);
        testList.add(t1);
        testList.add(t2);
        testList.add(t3);
        testList.add(t4);
        testList.add(t5);
        testList.add(t6);
        testList.add(t7);
        testList.add(t8);
        testList.add(t9);

        // 设置小票模型菜品集合
        Prient.setTestList(testList);
        // 定义纸张高度
//        int height = 200 + (testList.size() * 20) + 160;
        int height = 842;
        int width = 595;
        // 通俗理解就是书、文档
        Book book = new Book();
        // 打印格式
        PageFormat pf = new PageFormat();
        // 原点在纸张的左上方，x 指向右方，y 指向下方。
        pf.setOrientation(PageFormat.PORTRAIT);
        // 通过Paper设置页面的空白边距和可打印区域。必须与实际打印纸张大小相符。
        Paper p = new Paper();
        // 页面宽度 页面高度
        p.setSize(width, height);
        // x轴 y轴 宽度 高度
        p.setImageableArea(0, 0, width, height);
        pf.setPaper(p);
        // 把 PageFormat 和 Printable 添加到书中，组成一个页面
        book.append(new Prient(), pf);
        // 指定打印机打印（printerName打印机名称）
        HashAttributeSet hs = new HashAttributeSet();
        //打印机名称
        //获取默认打印机
        PrintService lookupDefaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
//        String printerName = "OneNote";
//        String printerName = "\\\\LCMPRINT01P\\LCM-Office-Printer";
        String printerName = lookupDefaultPrintService.getName();
        hs.add(new PrinterName(printerName, null));
        hs.add(MediaName.ISO_A4_TRANSPARENT);
        PrintService[] pss = PrintServiceLookup.lookupPrintServices(null, hs);
        for (PrintService printService : pss) {
            System.out.println("打印机列表: " + printService);
        }
        if (pss.length == 0) {
            System.out.println("无法找到打印机:" + printerName);
            return;
        }
        // 获取打印服务对象
        PrinterJob job = PrinterJob.getPrinterJob();
        // 写入书
        job.setPageable(book);
        try {
            Integer random6 = (int) ((Math.random() * 9 + 1) * 100000);
            System.out.println("使用的打印机为:" + pss[0]);
            System.out.println("Z" + System.currentTimeMillis() + random6.toString());
            // 添加指定的打印机
//            job.setPrintService(printService);
            job.setPrintService(pss[0]);
            // 打印执行
            job.print();
        } catch (PrinterException e) {
            System.out.println("================打印出现异常");
        }
    }
}