import javax.print.*;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.standard.MediaName;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @description:输入路径,打印pdf文件
 * @author: TC Zhai
 * @create: 2021-11-02 11:22
 **/
public class test {
    public static void main(String[] args) throws Exception {
        defaultPrintPDF("C:\\Users\\tczhai\\Desktop\\改动.pdf");
//        print2("C:\\Users\\tczhai\\Desktop\\改动.pdf", "140.206.75.42");
    }

    /**
     * 通过 IP+端口 连接打印机打印文件
     *
     * @param filePath
     * @throws Exception
     */
    public static void print2(String filePath, String ip) throws Exception {
        File file = new File(filePath); // 获取选择的文件
        Socket socket = new Socket(ip, 9100);

        OutputStream out = socket.getOutputStream();
        FileInputStream fis = new FileInputStream(file);
        //建立数组
        byte[] buf = new byte[1024];
        int len = 0;
        //判断是否读到文件末尾
        while ((len = fis.read(buf)) != -1) {
            out.write(buf, 0, len);
        }
        //告诉服务端，文件已传输完毕
        socket.shutdownOutput();
        socket.close();
        fis.close();
        System.out.println("打印完毕");
    }

    /**
     * 通过本机默认打印机打印pdf文件
     *
     * @param filePath 文件路径
     * @throws Exception
     */
    public static void defaultPrintPDF(String filePath) throws Exception {
        System.out.println("打印工具类入參：filePath===================" + filePath);

        File file = new File(filePath); // 获取选择的文件
        // 构建打印请求属性集
        HashPrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        // 设置打印格式，因为未确定类型，所以选择autosense
        DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
        System.out.println("打印文件类型为：===================" + flavor);
        //A4纸张
        pras.add(MediaName.ISO_A4_TRANSPARENT);
        PrintService printServices[] = PrintServiceLookup.lookupPrintServices(flavor, pras);
        //遍历
        for (PrintService printService : printServices) {
            System.out.println("本机可使用打印机列表：===================" + printService);
        }
        // 定位默认的打印服务
        PrintService defaultService = PrintServiceLookup
                .lookupDefaultPrintService();
        System.out.println("打印工具选择打印机为：===================" + defaultService);

        try {
            DocPrintJob job = defaultService.createPrintJob(); // 创建打印作业
            FileInputStream fis = new FileInputStream(file); // 构造待打印的文件流
            DocAttributeSet das = new HashDocAttributeSet();
            Doc doc = new SimpleDoc(fis, flavor, das);
            job.print(doc, pras);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("打印异常");
            throw new Exception();
        }
    }
}
